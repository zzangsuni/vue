#VUE 소개

##VUE 설치

* NPM 명령어
* VUE2와 VUE3 차이점

##VUE 구조
* template
* script
* style

##DIRECTIVE
* v-text, v-html
* v-bind
* v-model
* v-show, v-if, v-else, v-else-if
* v-for

##INSTANCE
* new Vue()

##VUE DOM
* Virture DOM
* render 함수

##VUE 라이프사이클(LIFECYCLE)

##Template Syntax

* Mothods
* Computed
* Watch
* Filter

##COMPONENT

* Global
* Local
* props 속성

##EVENT
* v-click
* 키보드 수식어
* 마우스 수식어


##STYLE

##SLOT

##AXIOS
* promise

##ROUTER
* 선언방법
```html
<router-view></router-view>
<router-link></router-link>
``` 
* path정의
    * 하위 path 정의
* name 속성
* parameter
    * params
    * query
* 라우터 인스턴스
* 멀티 라우터 뷰
* 리다이렉트
* 기타 고급기법

##VUEX
* 선언방법
* state
* mutations
    * commit
* actions
    * dispatch
* getters


##VUE와 ES6
* let 과 const
* arrow function
* 객체 리터럴
* 스프레드 오퍼레이터
* 템플릿 리터럴
* 디스트럭처링
* import & export
* async & await
