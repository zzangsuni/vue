# 자바스크립트 성능 최적화

DOM 이란,

- 서로다른 벤더사(IE5, NS6, OPERA)의 DHTML을 통일한 것.
- ECMA-262 3판때 자바스크립트 표준화함.
- 자바스크립트 엔진 변화 (2008년 이후 크롬 시작으로)
  - 크롬 - V8 (자바스크립트를 기계어로 변환, JIT(Just In Time)탑재)
  - 사파리4 - Nitro
  - 파이어폭스 3.5 - TraceMonkey

Javascript의 성능을 좌우하는 요소

- 실행시간
- 자바스크립트 내려받기
- DOM상호작용
- 페이지 라이프사이클

## 로딩과 실행

## 문제1. 자바스크립트는 내려받고 실행되는 동안, 다른 작업을 차단함.

<script> 구문를 만나기만 해도 다른 작업이 차단되는데, 스크립트에서 DOM의 수정이 발생할 수 있기 때문에
분석 전에는 다른 작업이 중담됨.
또한, <link>태그 뒤에 인라인 스크립트를 작성할 시, 스타일을 완전히 내려받고 인라인스크립트를 실행하기 때문에
페이지 내에 인라인 스크립트 작성 지양.

## 해결

- <script> 갯수를 줄여 자바스크립트 차단성을 줄임.
- HTTP 요청을 줄임 (코드를 하나로 묶어서 요청).
- 스크립트를 </body>앞에 정의.

## 문제2. 많은 양의 자바스크립트를 점진적 다운로드 방법 필요 (비차단법).

## 해결 

  동적으로 필요한 코드를 작성한 후, 페이지를 표시하는데필요한 코드를 작성. </body>앞에 loadScript함수를 실행하고
  다음에 DOM을 조작하는 코드 실행.

  - defer : 스크립트 내에 DOM을 조작하는 내용이 없다는 전제하에 나중에 호출해도 무관함을 명시하면 다른 프로레스를 차단하지 않음.
    - DOM이 완전히 로드 되기전에는 실행하지 않고, onload 이벤트 핸들러 호출 전에는 실행함.
  - <script> 태그를 스크립트 내에서 생성하여 src를 정의하는 방법.
    - 다른 프로세스를 차단하지 않음.
    - head에 두어도 잘 작동함.
  - 다른 스크립트의 인터페이스 코드일 때, 동적 스크립트의 사용시점 이용. IE는 *onreadystatechange* 이벤트, 타브라우저는 *onload* 사용.
    - loadScript 함수 생성  
```javascript
    function loadScript(url, callback){
      var script = document.createElement("script");
      script.type = "text/javascript";

      if(script.readyState){ //IE
        script.onreadystatechange = function(){
          if(script.readyState == "loaded" || script.readyState == "complete"){
            script.onreadystatechange = null;
            callback();
          }
        };
      }else { //타 브라우저
        script.onload = function(){
          callback();
        };
      }

      script.src = url;
      document.getElementByTagName("head")[0].appendChild(script);
    }
```  
    
- XMLHttpRequest 이용
외부에서 스크립트를 내려받기 때문에 바로 실행하지않아도 되고 실행 시점을 지정할 수 있음. 또 예외처리 없이 모든 브라우저에서 실행가능.  
단점은, 같은 도메인의 파일이 아닐 경우 CDN 파일을 사용할 수 없음.


```javascript

    var xhr = new XMLHttpRequest();
    xhr.open("get", "file1.js", true);
    xhr.onreadystatechange - function(){
      if(xhr.readyState == 4) {
        if(xhr.status >= 200 && xhr.status < 300 || xhr.status == 304) {
          var script = document.createElement("script");
          script.type = "text/javascript";
          script.text = xhr.responseText;
          document.body.appendChild(script);
        }
      }
    };
    xhr.send(null);

```  

- 기타 라이브러리 사용
    - YUI 3
    - LazyLoad 
    - LABjs

## 데이터 접근
 데이터를 어디에 저장하는 지에 따라 성능이 좋아짐.

- 리터럴 값(문자열, 숫자, 불리언, 객체, 배열, 함수, 정규표현식, null, undefined)
- 변수
- 배열항목 (리터럴과 변수보다 상대적으로 느림)
- 객체멤버 (리터럴과 변수보다 상대적으로 느림)

### 스코프
함수가 어떤 변수에 접근할 수 있는가,this의 값을 어떻게 할당하는가를 의미. 
즉, 데이터를 어디에 저장하여 접근하느냐에 성능에 영향을 줌.

함수 -> 내부속성[[scope]] 에는 (스코프 객체의 컬렉션 = 함수의 스코프 체인)를 포함.  
함수(실행) -> 실행문맥 생성 -> 실행문맥의 스코프체인은 실행중인 함수의 [[scope]] 속성으로 초기화
-> 활성화 객체(지역변수, 매개변수, arguments, this 포함)가 실행문맥에 맨 앞자리에 생성.
그 다음에 전역객체의 참조가 담겨져 있음.

*\*스코프 체인 : 함수가 접근할 수 있는 데이터 결정.

- 지역변수에 전역객체를 저장하여, 전역객체에 직접적인 참조를 최소화함.
- try-catch 에서 catch절에서 지역변수 객체의 순서가 두번째로 밀리게 되어, catch절에서 처리내용을
축소화 하고 다른 메서드로 에러처리를 위임하는 것이 좋음.
- 동적스코프 지양
    - with
    - try-catch
    - eval

### 클로저
 중첩함수에서 내부함수가 외부함수의 멤버변수에 접근을 하는 내부함수를 의미하는데,
 함수가 실행될때 참조하게되는 실행문맥(활성화객체)과 전역객체를 클로저가 공유하게 되면서
 실행이 끝나면 실행문맥이 파괴되는데, 클로저로 인해 파괴되지 않음. (메모리 누수발생)
 즉, 신중히 사용하는 것이 좋음.

### 프로토타입
 다른 객체의 기반이 되는 객체이고, 새 객체의 멤버를 정의해주는 것.
 Object를 제외한 내장객체(Date, Array 등)의 프로토타입은 Object가 됨.
 객체의 상속이 생기면 상속한 객체의 내부속성을 모두 공유받는데 즉, 프로토타입 체인이 형성됨.
 이 상속의 깊이가 깊을수록 성능이 느려짐(객체를 사용하지 않아야 함).
 또, 객체 메서드는 지역변수에 저장하지 않아야 함. 이렇게 되면 this가 윈도우에 묶이게 됨.

## DOM 스크립팅
스크립에서 DOM에 접근하는 횟수가 많을 수록 성능이 떨어짐.

- innerHTML이 createElement보다 빠르며, cloneNode가 createElement보다 빠름.
- HTML 컬렉션은 화면 구조가 변경되면 동적으로 내용이 자바스크립트에 반영됨.
.length로 루프문을 돌리게 되면 값이 갱신되어 무한루프에 빠질 수 있음. 지역변수에 length값을 저장하여 사용.
  - HTML컬렉션을 반환하는 메서드
    - document.getElementsByName()
    - document.getElementsByClassName()
    - document.getElementsByTagName()
    - document.images
    - document.links
    - document.forms
    - document.forms[0].elements
- 요소의 자식 요소를 재귀적이지 않은 방법으로 접근, nextSibling와 childNodes중 nextSibling은 IE에서 빠름
- DOM에서 주석이나 공백도 노드로 보고 이를 구별하려는 코드는 좋지 않음. 요소노드를 구별하여 반환하는 속성 사용.
    - children (childNodes 대체)
    - childElementCount (childNodes.length 대체)
    - firstElementChild (firstChild 대체)
    - lastElementChild (lastChild 대체)
    - nextElementSibling (nextSibling 대체)
    - previousElementSibling (previousSibling)
- querySelectorAll()이 getElementsByTagName()는 HTML컬렉션으로 속도가 느림.
### 리페인트와 리플로우
- DOM트리 : 페이지 구조를 나타냄
- 렌더트리 : DOM노드를 어떻게 표시할 지 나타냄.
- 리플로우 : 렌더트리를 다시 그리는 것으로 DOM의 요소의 변경이나 너비, 위치가 바뀌면 발생.
- 리페인트 : 리플로우가 끝나면 화면에 반영하기 위해 다시 그리는 것.
  (스타일에서 배경색을 바뀌면 리플로우 과정은 생략되고 리페인트만 일어남.)
- 리플로우 발생
 요소나 브라우저의 크기가 변하거나 페이지를 처음로드할 때 리플로우가 발생하는데 전체 플로우는 스크롤이 없다 발생할 때 일어남(성능이슈 발생).
 리플로우를 최적화 하기위해 렌더트리 변경을 큐에 저장해서 한번에 반영하는데, 큐를 비우는 행위를 피해야함.
    - 큐를 비우게 하는 속성
        - offsetTop, offsetLeft, offsetWidth, offsetHeight
        - scrollTop, scrollLeft, scrollWidth, scrollHeight
        - clientTop, clientLeft, clientWidth, clientHeight
        - getComputedStyle() or currentStyle(IE)
- 리플로우와 리페인트 최소화
    - DOM 스타일변경을 한번에 처리
        - style.cssText 속성 이용
          element.style.cssText = 'border-left:1px;border-top:2px;border-right:3px;';
        - element.className 을 클래스명을 변경
    - DOM 변경을 한번에 처리
        1. 요소를 숨기고 변경 후 다시 노출 (display 속성 이용)
        1. 현재 DOM 바깥에서 조각을 만들어 변경한 후 문서에 복사
        1. 현재 요소를 문서 밖의 노트에 복사해서 사본을 변경, 원래 요소 대체

```javascript
  //2번 예제
  var fragment = document.createDocumentFragment();
  appendDataToElement(fragment.data);
  document.getElementById('mylist').appendChild(fragment);

  //3번 예제
  var old = document.getElementById('mylist');
  var clone = old.cloneNode(true);
  appendDataToElement(clone, data);
  old.parentNode.replaceChild(clone, old);
```
- offset 같은 정보는 저장해두었다가 사용.
- 애니메이션할 때 요소를 흐름밖으로 꺼내기 
    1. 애니메이트 하려는 요소에 절대 위치(position 이용)를 지정해서 페이지의 레이아웃 흐름에서 뺌
    1. 요소를 애니메이트 시킴, 요소가 확장되면서 잠시 다른 부분 위에 겹침. 겹치면서 리페인트가 일어나지만 
    페이지의 일부분일 뿐이고 큰 부분에서 리플로우나 리페이트가 일어나지 않음.
    1. 애니메이션이 끝나면 요소의 포지션 속성을 원래대로 돌려서 문서의 나머지 부분을 한번만 밀어냄
- IE에서는 :hover를 많이 사용하면 응답성이 떨어짐. 많은 요소에는 사용하지 않은 것이 좋음.
- 요소에 이벤트 핸들러를 붙이는 데도 시간이 소요됨. 이벤트 핸들러를 붙이는 과정은 웹페이지가 분주한
onload나 DOMContentReady 이벤트에서 처리하기 때문에 많은 요소에 이벤트 핸들러를 붙이는 것은 좋지 않음.
즉, 이벤트 버블링을 이용해 부모요소에 이벤트 핸들러를 붙여 자식요소의 이벤트를 캐치하여 처리(이벤트 위임).

## 알고리즘
- 루프 (for, while, do-while 속도는 거의 비슷)
    - for
    - while
    - do-while
    - for-in (상속된 객체까지 탐색하므로 느림, 배열에서 사용하면 안됨.)
- 속도 줄이기
    - 객체 멤버와 항목검색 줄이기 (지역변수에 저장해서 사용)
    - 루프를 거꾸로 실행, 조건이 0이 아닌지 평가해서 비교를 두번에서 한번으로 줄임.
```javascript
  for(var i=items.length; i--;){
    process(items[i]);
  }

  var j=items.length;
  while(j--){
    process(items[j]);
  }

  var k=items.length-1;
  do {
    process(items[k]);
  }while(k--);

  //위의 처리방식은 일반적인 방법보다 두 단계(6단계 -> 4단계)를 줄임.
  //1. 실행 조건에서 비교 한번 (i==true)
  //2. 감소 연산자 한번(i--)
  //3. 배열 검색 한번 (item[i])
  //4. 함수 호출 한번 (process(items[i]))
```
- 반복줄이기
    - 제프그린버그 방식, 여러번 걸쳐서 처리할 것을 펼쳐 한번만 실행
    - 함수반복문(forEach) 는 루프보다 느림. 실행 속도가 중요할 때 비적합.
    - 조건의 수가 많아지면 if-else보다 switch가 적합
    - if-else 최적화
        - 가장 많이 쓰이는 조건을 맨앞에 둠
        - 조건문이 많으면 중첩 조건문을 사용

```javascript
  //제프 그린버그
  var iterations = items.length % 8, i = items.length - 1;
  while(iterations){
    process(items[i--]);
    iterations--;
  }

  iterations = Math.floor(items.length / 8);

  while(iterations){
    process(items[i--]);
    process(items[i--]);
    process(items[i--]);
    process(items[i--]);
    process(items[i--]);
    process(items[i--]);
    process(items[i--]);
    process(items[i--]);
    iterations--;
  }

  //중첩 if-else문
  //10번까지 조건을 평가하는 경우, 비중첩일때 모든 조건을 탐색해야 함.
  if(value < 6) {
    if(value < 3) {
      if(value == 0) {
        return result0;
      }else if(value == 1) {
        return result1;
      }else {
        return result2;
      }
    }else {
      if(value == 3) {
        return result3;
      }else if(value == 4) {
        return result4;
      }else {
        return result5;
      }
    }
  }else {
    if (value < 8) {
      if (value == 6) {
        return result6;
      }else {
        return result7;
      }
    }else {
      if(value == 8){
        return result8;
      }else if (value == 9) {
        return result9;
      }else {
        return result10;
      }
    }
  }
```
- 참조 테이블
  키와 값 형태로 1:1 대응관계일 때 유용, 키에 따라 고유 동작이 필요하거나 여러 동작을 묶을 때는 switch가 적합
```javascript
  var results = [result0, result1, result2, result3, result4, result5, result6, result7, result8, result9, result10];
  result results[value];
```
- 재귀 사용 비권장
자기 스스로를 호출하는 함수.
자칫 브라우저의 콜스택 제한을 초과할 수도 있음. 종료 조건을 잘못 또는 종료 조건을 명시하지 않은 경우 응답이 없을 수도 있음.
(콜스택은 브라우저의 자바스크립트 엔진마다 다름. try-catch로 대응할 수도 있으나 하지 않는 것이 좋음.)
    - 브라우저마다 콜스택 에러 메시지
        - 인터넷 익스플러로 : Stack overflow at line x
        - 파이어폭스 : Too much recursion
        - 사파리 : Maximum call stack size exceeded
        - 오페라 : Abort (control stack overflow)
        - 오페라11 : Uncaught exception : RangError : Maximum recursion depth exceeded
- 재귀 대체법
    - 반복문 사용
    - 메모이제이션
```javascript
//메모이제이션
function factorial(n){
  if (n == 0) {
    return 1;
  }else {
    return n * factorial(n-1);
  }
}

function memoize(fundamental, cache){
  cache = cache || {};
  var shell = function(arg){
    if (!cache.hasOwnProperty(arg)){
      cache[arg] = fundamental(arg);
    }
    return cache[arg];
  };
  return shell;
}

var memfactorial = memoize(factorial, {"0" : 1, "1" : 1});

var fac6 = memfactorial(6);
var fac5 = memfactorial(5);
var fac4 = memfactorial(4);
```

## 문자열
문자열을 합칠 때, 긴 문자열을 합치는데도 성능 이슈가 생김. join이나 concat메소드보다 +, +=를 사용하는 것이 빠름 (ie7이하는 느림).

## 응답성 좋은 인터페이스
- 자바스크립트를 빠르게 실행되도록 구현하는 것이 관건 (자바스크립트 동작 하나에 최대 0.1초 안에 구현)
- 0.1초 이상 걸리는 자바스크립트는 타이머 코드(setTimeout, setInterval)로 개선할 수 있음.
* 타이머 코드의 시간은 언제 큐에 추가할 것인지 정하는 것이지, 언제 실행하는 것을 정하는 것이 아님.
- 운영체제마다 타이머를 체크하는 시간이 다르며(윈도우는 15ms) 10ms이하로 설정되면 다른 동작을 보임.
 최소 25ms로 설정하는 것이 좋음.
- 50ms 이상 연속실행 하지 않음.
```javascript
  function timeProcessArray(items, process, callback) {
    var todo = items.concat();
    
    setTimeout(function(){
      var start = +new Date();
      do {
        process(todo.shift());
      } while(todo.length > 0 && (+new Date() - start < 50));

      if(todo.length > 0) {
        setTimeout(arguments.callee, 25);
      }else {
        callback(items);
      }
    }, 25)
  }

  var items = [123, 789, 323, 778, 232, 654, 219, 543, 321, 160];

  function outputValue(value) {
    console.log(value);
  }

  timeProcessArray(items, outputValue, function(){
    console.log('Done!');
  });
```
- 웹워커 (UI스레드 밖에서 처리, 느린처리를 웹워커에서 처리하면 다른 프로세스를 방해하지 않음.)
    - 워커 구성
        - navigator객체, 이 객체에는 appName, appVersion, user Agent, platform 네 가지 속성
        - location 객체, 이 객체는 window 객체와 같지만 일부 속성은 읽기전용
        - self 객체, 전역 worker 객체를 가리킴.
        - importScripts() 메서드, 외부 자바스크립트를 불러와서 워커에서 사용
        - 모든 ECMAScript 객체, Object, Array, Date등
        - XMLHttpRequest 생성자
        - setTimeout(), setInterval() 메서드
        - close() 메서드, 워커를 즉시 중지
```javascript
  //JSON 문자열을 파싱

  var worker = new Worker('jsonparser.js);
  //데이터를 이용할 수 있게 되면 이 이벤트 핸들러를 호출
  worker.onmessage = function(event){
    //JSON 구조를 넘겨받음
    var jsonData = event.data;
    //JSON 구조를 사용.
    evaluateData(jsonData);
  };
  //파싱할, 매우 큰 JSON 문자열을 넘김.
  worker.postMessage(jsonText);

  //jsonparse.js 내부
  //JSON 데이터를 이용할 수 있게 되면 이 이벤트 핸들러를 호출
  self.onmessage = function(event){
    //JSON 문자열은 event.data로 전달
    var jsonText = event.data;
    //구조를 파싱
    var jsonData = JSON.parse(jsonText);
    //결과를 돌려보냄.
    self.postMessage(jsonData);
  }
```
## Ajax 사용
## 프로그래밍 사례
### 사용지양 코드
1. Function() 생성자 - 필요성이 많지 않음.
1. setTimeout(), setInterval() - 사용시 함수를 사용하고 문자열을 사용하지 않음.
1. eval() - 프로그램이 느려짐

### 객체와 배열 선언
생성사를 사용하여 객체와 배열을 선언하지 않고, 리터럴 방식을 사용.
```javascript
var obj = new Object(); //느림
var obj = {  //훨씬 빠름
  name : 'eunjoung'
};
```
### 작업을 반복하지 않음
1. 게으른 로딩
1. 조건에 따른 미리 읽기
예) 사용할 수 있는 속성인지 체크하고 처리하는 함수구현시
*게으른 로딩*
프로그램이 실행되는 동안 매번 속성체크를 해서 if-else 구문을 실행하도록 하지않고,
사용할 수 있는 속성은 처음 한번만 하면 프로그램 내내 변경될 일이 없음.
가능한 속성이 한번 체크되면 해당하는 구문으로 기존 함수의 내용을 다시 명시하여 덮어씀.
*조건에 따른 미리 읽기*
삼항식을 사용하여, 처음부터 속성을 체크해서 해당하는 해당 구문으로 바로 이동하도록 처리.

### 비트연산자 사용
- 비트연산자 (AND, OR, XOR, NOT, >>, <<, >>>, <<<)의 연산이 빠름
- 내장함수 사용 (Math, querySelector 등), 사용자가 만들어 사용하는 것보다 빠름  
*숫자를 toString()으로 출력하면 2진법으로 변환하여 출력함*

### 배포 및 빌드
- HTTP 요청 횟수 줄임
- 자바스크립트 파일 최소화
- 자바스크립트 파일 압축
- 자바스크립트 캐시 사용 및 업데이트를 위해 타임스탬프로 문제 해결.
- CDN을 이용.
