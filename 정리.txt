1. this.$emit('send:event'); => v-on:send:event 와 같이 카멜(js)&케밥(html) 명명규칙보다는
    "행위:대상" 형태로 작성하는 것이 최신스타일임. ==> 버전업이 되면서 카멜&케밥 모두가능. (2.5버전 지원X)

2. 컴포넌트명을 작성할 때는 두단어를 조합하여 작성한다. 기존에 html의 태그명과 충돌할 경우가 
    있을 수 있기 때문에 스타일가이드에 권고하는 사항임.

3. data(){return{}} 형식은 컴포넌트간의 데이터를 공유하지 않기위한 방식.
해당 방법은 cli 로 프로젝트를 구성했을때 사용.

4. npm init -y : 기본값으로 package.json이 세팅됨.

5. json에 정의된 값들은 key & value 모두 "" 으로 묶어야 함.
예) 
{
    "name": "npm"
}

6. npm i vue 에서 i는 install 의 약자
--save-dev 의 축약어는 '-D'

7. 설치시 권한이 없을 경우, npm 얖에 sudo를 작성.(admin 권한)

8. 강의기준 vue.js 2.9

9. vetur 단축키 
- scf => .vue 파일의 기본구조 생성
- doc => 기본 html5 구조생성

10. react 와 angular에는 computed와 watch를 제공하지 않음.

11. computed & watch & methods의 비교
computed는 data 속성의 값이 변경되었을때 다시 계산해서 수행값을 반환.
watch는 데이터 속성을 함수로 걸어 그 데이터의 변화를 직접감시함.
예)
data : function(){
    return {
        message : ''
    }
},
watch : {
   message : function(){
       //data message 와 같은 함수를 선언.
   } 
}
[부가정보] :
- data, computed, watch : Reactivity(반응성관련된 속성)
- methods : function


12. npm install과 npm run dev를 한줄코드로 줄일수 있음.
=> npm i & npm run dev

13. index.html 에 선언된 /dist/build.js는 실제 폴더구조상에 노출은 하지 않았지만
메모리상에 존재하여 사용함. 이 방법은 빠른 속도를 위한 처리.
하지만, npm run build하면 배포를 위해 파일이 노출됨.

14. mounted에서만 #app내부 html에 접근가능. (vm.$el을 생성 el속성을 대입.)

15. presentational container components

16. props : ['todoList'] 접근 : this.todoList 으로 데이터 가공가능.

17. v-bind:title="titleVal"  v-bind: 를 사용하면 titleVal은 변수로 인식
title="titleVal" 은 titleVal은 문자로 인식.

18. 한 컴포넌트에 여러 개의 이벤트 정의가능($emit관련)

19. webpack = 웹 태스크 매니저 + 파일의 내용을 불러오는 도구(amd, commonJs, requre.js)
- gulp & grunt
    - 자동 새로 고침
    - 배포전 작업 자동화
        - 압축
        - 주석 제거
        - 파일 합치기 (번들링)

20. webpack3 VS webpack4 차이
- version3(global 설치) : 
    npm install webpack -g
    npm install webpack@버전 

- version4(local 단위로 설치) : 
    npm install webpack --save-dev
    npm install webpack-cli --save-dev
        => npm install webpack webpack-cli --save-dev(축약)
        => webpack은 번들을 위한 기능을 담겨져 있고.
        => cli는 커맨드 라인의 입력을 받아 처리하는 기능이 담겨있음.
        => 커맨드로 처리하는 모든 것을 cli라 지칭함.

21. lodash : 많이 사용하는 api를 제공하는 플러그인
22. webpack --mode=none (명령어의 의미)
=> webpack.config.js 에서 설정을 관리할 경우 --mode=none은 필요없음.

23. 설정관련 
    - 맥에서는 package.json > scripts > ' "build" : "webpack --mode=none" ' 추가해서
    => npm run build 명령어로 가능.

24. 번들된 js 로그보는법
아래와 같이 index번호를 매겨 관리.
/* 0 */
코드
/* 1 */
코드

25. webpack에서 css를 관리하기위해서는 전용 loader가 필요.
=> 
1. css-loader, style-loader로 빌드하면 빌드해서 떨어진 .js파일에 css소스까지 포함됨.
2. mini-css-extract-plugin loader로 빌드하면, .css파일이 생성되어 css소스를 별도파일로 관리할 수 있음.

26. require 문법차이
//ES5
require('vue');
//ES6
import Vue from 'vue';

27. webpack-dev-server 자동빌드해주는 도구
=> package.json에 아래와 같이 정의되어 있어야함.
"scripts": {
    "dev" : "webpack-dev-server --open"
  }
=> 설정이 끝나면 npm run dev 명령어로 서버구동!!

28. package-lock.json : npm 의존성관리

29. Vuex 4버전으로 변경시, actions가 사라짐. (내년쯤?)

30. webpack build 시 .map파일 역할 : 개발자도구 < source탭에서 *.bundle.js를 열면
*.bundle.js파일에서 나는 오류를 *.bundle.js.map에서 가지고 있다가 보여줌.

31. arrow 함수 주의사항
- data, created, methods 안 함수등의 Vue API 메소드 정의는 ()=>{} 형식의 arrow함수를 작성하면 안됨.

32. vue cli 설치 : vue init webpack-simple vue-this
현재 경로에서 vue-this라는 폴더를 생성해서 webpack-simple 을 설치함.

33. created 훅에서 DOM에 접근하고자 할 경우, this.$refs.[ref value] 로 접근할수 있음.
예)
<template>
    <p ref="para"></p>
</template>
<script>
    export default {
        created : function(){
            this.$refs.para; //접근가능.
        }
    }
</script>

34. v-on:click 형식으로 이벤트를 걸면, vue에서 자동으로 이벤트를 해지해줌.
하지만 eventbus를 사용해서 eventBus.$on('') 로 이벤트를 걸때는 사용자가 직접 해지해야함.

35.Vuex
state(data)의 데이터를 조작하는 것은 mutations에서 담당하도록 설계.
state의 변경은 mutations를 통해서 변경하여야 함.

36.vue2 vs vue3 비교
[2.X]
- CLI 설치명령어 : npm i vue-cli -g
- 프로젝트 초기화 : vue init webpack-simple '프로젝트퐅더'
- 작업도중에 필요한 도구를 추가할 경우 처음부터 다시 세팅해야함.
- 명령어 순서
    - vue init webpack-simple vue-news
    - cd vue-news
    - npm i
    - npm run dev
- 웹팩 설정파일 노출 O

[3.X]
- CLI 설치명령어 : npm i @vue/cli -g
- 프로젝트 초기화 : vue create '프로젝트퐅더'
- 작업도중에 필요한 도구를 플러그인 형식으로 추가할 수 있음.
- 명령어 순서
    - vue create vue-news
    - cd vue-news
    - npm run serve
- 웹팩 설정파일 노출 X
- 웹팩을 vue3에서 설정하는 방법 (https://cli.vuejs.org/guide/webpack.html#simple-configuration)

37. 
1. 라우터 설치
npm i vue-router --save

2. 라우터 인스턴스 생성
new VueRouter()

3. 라우터 인스턴스안에 옵션을 정의
 - url 정의
 - url 경로에 해당하는 컴포넌트
    routes: []

4) 라우터 인스턴스를 뷰 인스턴스에 넣어줌
new Vue({
  router: router
})

5) 라우터를 표시하는 태그
<router-view></router-view>

6) 특정 url로 이동하는 태그
<router-link></router-link>

38. Vue.config.productionTip = false 의미

39. 데이터를 불러와 조작하는 시점, created 와 beforeMount 시점임.

40. vue.js 입무자가 저지르는 실수 5가지
    1)  인스턴스가 생성된 후 동적으로 데이터(속성)를 추가했을 경우, watcher가 붙지않아 원하는 결과가 나오지 않음.
        - Vue.set(this.user, 'regiion', 'Pangyo'); //속성을 이용하여 추가해야함.
        - 아니면, 사용할 속성을 미리 data영역에 정의해두어도 됨.
        예) E:\fastcampus\vue-camp-master\3_vuex\five-common-mistakes\1_reactivity-caveats.html

    2) 반복적인 형태의 (일종의 배열값을 뿌리는 형태) 경우에는 ref속성을 정의하여, 요소에 쉽게 접근하여 조작할수 있음.

41. //Object.defineProperty() : 객체에 직접 새로운 속성을 정의하거나 이미 존재하는 객체를 수정한 뒤 그 객체를 반환
    // var viewModel = {};
    // var value = 'hello';
    // Object.defineProperty(viewModel, 'str', {
    //   get : function(){
    //     console.log(value)
    //   },
    //   set : function(newValue){
    //     render(newValue);
    //     value = newValue;
    //   }
    // });

42. 전역등록 방식은 실무에서 잘 사용하지 않음.
Vue.componenet('[componenet name]',{

})

43. 플러그인 사용법 [참조]
E:\fastcampus\vue-camp-master\2_todo\chart-with-plugin\exercise\src\plugins\ChartPlugin.js

44. 서비스 구현시, 폴도 구조
src
└ components : 공통영역에 사용되는(재사용) 파일
└ plugins : 외부플러그인 파일
└ api : API함수 정의
└ assets : 이미지, CSS 등 파일
 └ css
 └ img
└ views : 화면단들

45.npm i babel-plugin-transform-runtime

46. new Promise().then() 대신에
async & await (es6문법) 사용.
예) async function test(){
    var result = await axios.get('');
    console.log(result);
}


/***********************************************************************/
/*******************************질문************************************/
/***********************************************************************/
1. vuex에서 helper함수를 사용하는 케이스
2. 비동기처리시, componenet, mutations, actions에 각각 메서드를 생성해야하는데 명명규칙을 어떻게하는게 효율적일까?
3. 아래두 차이점

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})


new Vue({
  store,
  render: h => h(App)
}).$mount('#app')

4. v-for 문에서 선언해야하는 :key 바인딩을 사용자가 사용하는 경우도 있는지 
=> 거의없음
